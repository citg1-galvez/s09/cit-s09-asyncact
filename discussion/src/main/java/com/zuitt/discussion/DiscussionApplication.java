package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greetings")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	@GetMapping("/hello")
	public String hello() {
		return "Hello World";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s My name is %s.", friend, name);
	}

	// Route with path variables
	// Dynamic is obtained directly from the url
	// localhost:8080/name
	@GetMapping("/hello/{name}")
	// "PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}

// Activity

	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Brett") String user) {
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "Brett") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id) {
		switch(id){
			case "java101":
				return String.format("Name: Java 101,Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000.00");
			case "sql101":
				return String.format("Name: SQL 101, Schedule: TTH 1:00PM-04:00PM, Price: PHP 2000.00");
			case "javaee101":
				return String.format("Name: Java EE 101, Schedule: MFW 1:00PM-04:00PM, Price: PHP 3500.00");
			default:
				return String.format("Course cannot be found.");
		}
	}

    // Async Activity

	// Student Class
	private class Student {
		private String id;
		private String name;
		private String course;

		public Student(String id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		public String getCourse() {
			return course;
		}

		public void setCourse(String course) {
			this.course = course;
		}
	}

	//Student Array
	private ArrayList<Student> students = new ArrayList<>();

	//Routes
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value = "user") String user, @RequestParam(value = "role") String role) {

		switch (role) {
			case "admin":
				return String.format("<h1>Welcome back to the class portal, Admin %s!</h1>", user);
			case "teacher":
				return String.format("<h1>Thank you for logging in, Teacher %s!</h1>", user);
			case "student":
				return String.format("<h1>Welcome to the class portal, %s!</h1>", user);
			default:
				return "<h1>Role out of range!</h1>";
		}
	}

	@GetMapping("/register")
	public String register(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam(value = "course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("<h1>%s, your id number is registered on the system!</h1>", id);
	}

	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		for (Student student : students) {
			if (student.getId().equals(id)) {
				return String.format("<h1>Welcome back, %s! You are currently enrolled in %s.</h1>", student.getName(), student.getCourse());
			}
		}
		return String.format("<h1>You provided %s is not found in the system!</h1>", id);
	}



}
